gdb image, for debugging applications with xtensa32 architecture.


Volumes:
PROJECT: Mapped to /project in the container, contains files to expose to
  gdb (if needed). This is entirely optional and needn't be supplied when no
  files have to be shared with gdb.

Documentation:
For the underlying gdb project, see https://www.sourceware.org/gdb/
For this container, see https://gitlab.com/c8160/embedded-rust/gdb-xtensa32

Requirements:
None

Configuration:
None

